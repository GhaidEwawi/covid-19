#!/usr/bin/env python
# coding: utf-8

# In[37]:


# All needed libraries
import pandas as pd
import numpy as np
import sklearn
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
sns.set() # For better graphs.

# Our models
from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPRegressor
from sklearn.svm import SVR

# The array will contain the names and accuracy of the models. 
modelPerformance = pd.DataFrame(columns=['Model', 'Test Score', 'Train Score'])

countryName = 'World'

# The function that reads the data from CSV file.
def readData(countryName=countryName, nDays=101, fileName='total_cases.csv'):
    data = pd.read_csv(fileName)
    data = data.fillna(0)
    # This line might be used when I want to discard some of the data
    # lastZero = np.argmin(data[countryName][::-1]) 
    my_data = data.loc[:nDays, [countryName]]
    my_data['day'] = pd.DataFrame(np.arange(0, nDays+1))[0]
    return my_data

# Splitting the data with the same random state.
def splitData(data):
    X = data['day'].values.reshape(-1, 1)
    y = data[countryName].values.reshape(-1, 1).ravel()
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state=42)
    return X_train, X_test, y_train, y_test

# Storing the data in a variable
data = readData(nDays=102)

# Plotting the actual data
plt.xlabel('day')
plt.ylabel('number of infections')
plt.title('The actual Data')
plt.plot(data.loc[:, countryName])
plt.show()


############ Linear Regression ::::::
# If I train it from day number 70, I get better results.
startDay = 70
X = data.loc[startDay:, 'day'].values.reshape(-1,1)
y = np.ma.log(data.loc[startDay:, countryName].values).reshape(-1,1)
y = y.filled(0)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state=42)

reg = LinearRegression()
reg.fit(X_train, y_train);

modelPerformance = modelPerformance.append([ {'Model': 'Linear Regression',                                               'Test Score': r2_score(np.exp(reg.predict(X_test)), np.exp(y_test)).round(4),                                                'Train Score': r2_score(np.exp(reg.predict(X_train)), np.exp(y_train)).round(4)}])

# Plotting Linear Regression as exponential
plt.plot(np.arange(0, 102), np.exp(reg.predict(np.arange(0, 102).reshape(-1, 1))), color='grey', label='predicted')
plt.plot(data['day'].values, data[countryName].values, color='red', label='real')
plt.xlabel('Day'); plt.ylabel('Number of infections'); plt.title('Linear Regression / Infections Model'); plt.legend()
plt.show()

# Plotting Linear Regression as a linear relation
plt.scatter(X_train, y_train, color='green', s=3, label='train')
plt.scatter(X_test, y_test, color='blue', s=3, label='test')
plt.plot(np.arange(startDay, 102), (reg.predict(np.arange(startDay, 102).reshape(-1, 1))), color='grey')
plt.xlabel('Day'); plt.ylabel('logValue'); plt.title('Log Linear Regression'); plt.legend()
plt.show()

############ MLP Regressor ::::::
X_train, X_test, y_train, y_test = splitData(data)
mlp = MLPRegressor(solver='lbfgs', hidden_layer_sizes=200)
mlp.fit(X_train, y_train)

modelPerformance = modelPerformance.append([ {'Model': 'MLP',
                          'Test Score': mlp.score(X_test, y_test), 
                           'Train Score': mlp.score(X_train, y_train)}])

# Plotting MLP Regressor
plt.figure()
plt.plot(np.arange(0, 102), (mlp.predict(np.arange(0, 102).reshape(-1, 1))), color='grey', label='predicted')
plt.plot(data['day'].values, data[countryName].values, color='red', label='real')
plt.xlabel('Day'); plt.ylabel('Number of infections'); plt.title('MLP Regressor / Infections Model'); plt.legend();
plt.show()




############ SVR  ::::::

X_train, X_test, y_train, y_test = splitData(data)
from sklearn.svm import SVR
svr = SVR(degree=3, kernel='poly', max_iter=-1, gamma='auto', coef0=2.0)
svr.fit(X_train, y_train)
modelPerformance = modelPerformance.append([ {'Model': 'SVR',                                               'Test Score': svr.score(X_test, y_test),                                                'Train Score': svr.score(X_train, y_train)}])

# Plotting SVR Regressor
# Make predictions with our model
train_predictions = svr.predict(X_train)
test_predictions = svr.predict(X_test)

# Create a scatter plot with train and test actual vs predictions
plt.scatter(X_train, train_predictions, label='train'); plt.scatter(X_test, test_predictions, label='test')
plt.scatter(X_train, y_train, color='grey', label='actual-train', s=10); plt.scatter(X_test, y_test, label='actual-test', s=10)
plt.xlabel('Day'); plt.ylabel("Number of infections"); plt.title('SVR / Infections Model'); plt.legend()
plt.show()

print("\n\nModel Performances :")
print(modelPerformance)

######################################################################################
################ Death Predictions ###################################################
######################################################################################
DeathModelPerformance = pd.DataFrame(columns=['Model', 'Test Score', 'Train Score'])

deaths = readData(fileName='total_deaths.csv')


############ Linear Regression ::::::
startDay = 70
X = deaths.loc[startDay:, 'day'].values.reshape(-1,1)
y = np.ma.log(deaths.loc[startDay:, countryName].values).reshape(-1,1)
y = y.filled(0)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state=42)

reg = LinearRegression()

reg.fit(X_train, y_train);
DeathModelPerformance = DeathModelPerformance.append([ {'Model': 'Linear Regression',                                                         'Test Score': reg.score(X_test, y_test),                                                         'Train Score': reg.score(X_train, y_train)}])

# Plotting Linear Regression as exponential
plt.plot(np.arange(0, 102), np.exp(reg.predict(np.arange(0, 102).reshape(-1, 1))), color='grey', label='predicted')
plt.plot(deaths['day'].values, deaths[countryName].values, color='red', label='real')
plt.xlabel('Day'); plt.ylabel('Number of Deaths'); plt.title('Linear Regression / Deaths Model'); plt.legend()
plt.show()




############ MLP Regressor  ::::::
X_train, X_test, y_train, y_test = splitData(deaths)
mlp = MLPRegressor(solver='lbfgs', hidden_layer_sizes=200)
mlp.fit(X_train, y_train)
DeathModelPerformance = DeathModelPerformance.append([ {'Model': 'MLP',                                                         'Test Score': mlp.score(X_test, y_test),                                                         'Train Score': mlp.score(X_train, y_train)}])
# Plotting MLP Regressor
plt.figure()
plt.plot(np.arange(0, 102), (mlp.predict(np.arange(0, 102).reshape(-1, 1))), color='red', label='predicted')
plt.plot(deaths['day'].values, deaths[countryName].values, color='grey', label='real')
plt.xlabel('Day'); plt.ylabel("Number of Deaths"); plt.title('MLP Regressor / Deaths Model'); plt.legend()
plt.show()


############ SVR  ::::::
X_train, X_test, y_train, y_test = splitData(deaths)
svr = SVR(C=1, coef0=2, degree=3, epsilon=0.1, gamma=0.5, kernel='poly', max_iter=-1)
svr.fit(X_train, y_train)

DeathModelPerformance = DeathModelPerformance.append([ {'Model': 'SVR',                                                         'Test Score': svr.score(X_test, y_test),                                                         'Train Score': svr.score(X_train, y_train)}])

# Plotting SVR Regressor
plt.figure()
plt.plot(np.arange(0, 102), (svr.predict(np.arange(0, 102).reshape(-1, 1))), color='red', label='predicted')
plt.plot(deaths['day'].values, deaths[countryName].values, color='grey', label='real')
plt.xlabel('Day'); plt.ylabel("Number of Deaths"); plt.title('SVR / Deaths Model'); plt.legend()
plt.show()

print("\n\nDeaths Models Performances")
print(DeathModelPerformance)


# In[ ]:





# In[ ]:




